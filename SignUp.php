<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up For New Student</title>
</head>
<body>
    <div class="border" style = "border: 2px solid rgb(26, 117, 255);
    background-color: white;
    padding: 40px 30px 30px 30px;
    position: absolute;
    margin-left: 35%;
    margin-top: 15%;
    width: 25%;
    ">
        <div style=" margin-left: 15%;" class="form">
            <div class="name">
                <label style="background-color:rgb(102, 163, 255);
                padding : 10px 10px;
                color :white;
                border:2px solid rgb(26, 117, 255)" for="username">Họ và tên</label>
                <input style=" margin-left: 5%;
                border:2px solid rgb(102, 163, 255);
                height: 35px;
                width:60%"
                type="text"
                name="username"/>
            </div>
            <div style="margin-top:30px">
                <label style="background-color:rgb(102, 163, 255);
                padding : 10px 10px;
                color :white;
                border:2px solid rgb(26, 117, 255)" for="username">Giới tính</label>
                <?php
                $gender = array("Nam","Nữ");
                for ($i = 0; $i <= 1; $i++) {
                    if($i%2 == 0) {
                    echo '<input value="'.$gender[$i].'" id="'.$gender[$i].'" style="margin-left:30px" type="radio" name="'.$gender[$i].'">'.$gender[$i];
                    echo '<input value="'.$gender[$i+1].'" id="'.$gender[$i+1].'" style="margin-left:30px" type="radio" name="'.$gender[$i].'">'.$gender[$i+1];
                    }
                }
                ?>            
            </div>
            <div class = "subject">
                <label class="subject_selection" 
                style="background-color:rgb(102, 163, 255);
                padding : 10px 10px;
                color :white;
                border:2px solid rgb(26, 117, 255)">Phân khoa</label>
                <select name="subject" id="subjects" style=" margin-left: 5%; height: 40px; border:2px solid rgb(102, 163, 255);">
                <?php
                    $subject = array(" "=>" ","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học dữ liệu");
                    foreach($subject as $key => $val){
                        $selected = ($key == ' ') ? 'selected="selected"' : '';
                        echo '<option value="'. $key .'" ' . $selected . ' >'. $val .'</option>';
                    }
                ?>
                </select>
            </div>
                <div class="sign_up_button">
                    <button  style="background-color:green;
                    color :white;
                    padding :15px 40px;
                    margin-left: 23%;margin-top:40px;
                    border:2px solid rgb(26, 117, 255);
                    border-radius:15px;"> Đăng ký </button>
                </div>
        </div>
    </div>
</body>
</html>
<style>
    .subject{
        margin-top:30px;
    };
</style>